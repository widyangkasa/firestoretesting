

const initialState = {
    users: [
        {
            id: '123456789',
            name: 'Mario',
            address: 'MarioLand'
        }
    ]
}

export const reducer = (state = initialState, action) =>{
    switch(action.type){
        case 'ADD_USER':
            return{
                users: [...state.users, {id: action.id, name: action.name, address: action.address}]
            }
        case 'DELETE_USER':
            return {
                users: state.users.filter((user) => user.id != action.id)
            }
        case 'EDIT_USER':
            return {
                users: state.users.map((user)=> user.id == action.id? 
                {id: user.id, name: action.name, address: action.address }: user)
            }
        case 'REFRESH_USER':
            return {
                users: action.users
            }
        default: 
            return state
    }
}