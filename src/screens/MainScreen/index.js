import React, {Component} from 'react';
import { StyleSheet, View, Text, TextInput, Button} from 'react-native'
import { connect } from 'react-redux'
import { addUser, deleteUser, editUser, refreshUser } from './action';
import firestore from '@react-native-firebase/firestore';
import User from './User'
import { ScrollView } from 'react-native-gesture-handler';

class MainScreen extends Component {
    
    state= {
        id: '',
        name: '',
        address: ''
    }

    // getAllUsers =() => {

    // }

    async componentDidMount() {
        this.getAllUsers()
      }

    getAllUsers = async () => {
        let usersCollection = await firestore().collection('users').get();
        let users = []
        usersCollection.forEach(documentSnapshot => {
            console.log(documentSnapshot.id)
            let user = new User(documentSnapshot.id,documentSnapshot.data().name, documentSnapshot.data().address)
            console.log(user)
            users.push(user)
        })
        this.props.refreshUser(users);
    }
    
    handleChangeText =(text, inputName) =>{
        this.setState({
            ...this.state,
            [inputName]: text
        })
    }

    handleAddUser = async ()=> {
        try{
            let user = this.state;
            let response = await firestore().collection('users').add({name: this.state.name, address: this.state.address})
            // console.log("response", response.id); //??     //auto generated id
            await this.props.addUser({...user, id: response.id});
            this.setState({
                id: '',
                name: '',
                address: ''
            })
            // var newUserRef = firestore().collection('users').doc();
            // console.log("newUserref", newUserRef);   
        }catch (err){
            alert(err)
        }
    }

    handleEditUser = async () => {
        try{
            let user = this.state;
            let response = await firestore().collection('users').doc(this.state.id).update({name: this.state.name, address: this.state.address})
            // console.log("update response", response); //??     
            await this.props.editUser({...user, id: this.state.id});
            this.setState({
                id: '',
                name: '',
                address: ''
            })
        }catch (err){
            alert(err)
        }
    }

    handleDeleteUser = async (id) => {
        try{
            console.log("deleted id", id)
            let response = await firestore().collection('users').doc(id).delete()
            // console.log("delete response", response); //??    
            await this.props.deleteUser(id);
        }catch (err){
            alert(err)
        }
    }

    render() {
        const users = this.props.users
        return(
            <ScrollView>
                <Text>{JSON.stringify(this.state)}</Text>

                <Text>{JSON.stringify(this.props.users)}</Text>
                <TextInput placeholder="name" onChangeText={(text)=> this.handleChangeText(text, "name")} value={this.state.name}/>
                <TextInput placeholder="address" onChangeText={(text)=> this.handleChangeText(text, "address")} value={this.state.address}/>
                <Button title="Add User" onPress={()=> this.handleAddUser()}/>
                <Button title="Edit User" onPress={()=> this.handleEditUser()}/>
                {
                    users.map(user => (
                        <View>
                            <Text>{user.name}</Text>
                            <Text>{user.address}</Text>
                            <Button title="edit" onPress={()=> this.setState({
                                id: user.id,
                                name: user.name,
                                address: user.address
                            })}/>
                            <Button title="delete" onPress={()=> this.handleDeleteUser(user.id)}/>
                        </View>
                    ))
                }
            </ScrollView>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        users: state.users
    }
}

const mapDispathToProps = (dispatch) => {
    return {
        refreshUser: (users) => dispatch(refreshUser(users)),
        addUser: (user) => dispatch(addUser(user)),
        deleteUser: (id) => dispatch(deleteUser(id)),
        editUser: (user) => dispatch(editUser(user))
    }
}

export default connect(mapStateToProps, mapDispathToProps)(MainScreen)