const ADD_USER = 'ADD_USER';
const DELETE_USER = 'DELETE_USER';
const EDIT_USER = 'EDIT_USER';
const REFRESH_USER= 'REFRESH_USER';


export const addUser = (user) => {
    return {
        type: ADD_USER,
        id: user.id,
        name: user.name,
        address: user.address
    }
}

export const deleteUser = (id) => {
    return {
        type: DELETE_USER,
        id
    }
}
/** @param user object */
export const editUser = (user) => {
    return {
        type: EDIT_USER,
        id: user.id,
        name: user.name,
        address: user.address
    }
}

/**@param array of users*/
export const refreshUser = (users) => {
    return {
        type: REFRESH_USER,
        users
    }
}
