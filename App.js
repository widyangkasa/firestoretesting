import React, {Component} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import firestore from '@react-native-firebase/firestore';
import thunk from 'redux-thunk'
import {createStore, applyMiddleware} from 'redux'
import { Provider } from 'react-redux';
import MainSceen from './src/screens/MainScreen';
import {reducer} from './src/screens/MainScreen/reducer';
 
const store = createStore(reducer, applyMiddleware(thunk));

class App extends Component {

  render() {
    return(
      <Provider store={store}>
        <MainSceen/>
      </Provider>


    )
    
  }
}

export default App;